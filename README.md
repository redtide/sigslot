sigslot
=======

Sarah Thompson's [sigslot library](http://sigslot.sourceforge.net/)

plus some [required fixes](http://sourceforge.net/p/sigslot/patches/).

See also:

+ <https://github.com/catnapgames/SigSlot>
+ <https://github.com/anthnich/SigSlot>